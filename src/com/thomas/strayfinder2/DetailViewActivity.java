package com.thomas.strayfinder2;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.thomas.strayfinder2.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.MenuItem;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;

public class DetailViewActivity extends SherlockActivity {
	private WebView wv;

	private void drawWebView(String html) {
		wv.getSettings().setJavaScriptEnabled(true);
		wv.getSettings().setPluginState(PluginState.ON);
		wv.getSettings().setLoadsImagesAutomatically(true);

		html = "<link rel=\"stylesheet\" type=\"text/css\" href=\"file:///android_asset/style.css\" />"
				+ html;
		wv.loadDataWithBaseURL(null, html, "text/html", "UTF-8", null);
		wv.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_detailview);

		wv = (WebView) findViewById(R.id.webview_detail);

		Intent intent = getIntent();
		String status = intent.getStringExtra("status");
		String view_url = intent.getStringExtra("view_url");
		String base_url = getResources().getString(R.string.base_url);

		new DetailViewParser() {
			private String html = "";

			@Override
			protected void onPostExecute(Exception result) {
				super.onPostExecute(result);

				drawWebView(html);
			}

			@Override
			protected void onProgressUpdate(ParsedItem... values) {
				super.onProgressUpdate(values);

				html += values[0].toString();
			}
		}.execute(view_url, base_url, status);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.activity_detailview, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_share:
			Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);

			shareIntent
					.setType("text/plain")
					.putExtra(android.content.Intent.EXTRA_SUBJECT, "유기 동물 정보")
					.putExtra(android.content.Intent.EXTRA_TEXT,
							"유기견 공고 " + getIntent().getStringExtra("view_url"));

			startActivity(Intent.createChooser(shareIntent, "공유할 도구를 선택하세요"));

			break;
		}

		return super.onOptionsItemSelected(item);
	}
}