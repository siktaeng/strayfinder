/**
 * 
 */
package com.thomas.strayfinder2;

import com.actionbarsherlock.app.SherlockActivity;
import com.thomas.strayfinder2.R;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

/**
 * @author stk.lim
 * 
 */
public class AboutActivity extends SherlockActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);

		TextView tv = (TextView) findViewById(R.id.textview_version);
		tv.setMovementMethod(LinkMovementMethod.getInstance());

		String appname = getResources().getString(R.string.app_name);
		try {
			PackageManager pm = getPackageManager();
			PackageInfo packageInfo = pm.getPackageInfo(this.getPackageName(),
					0);

			tv.setText(String.format("%s ver%s", appname,
					packageInfo.versionName));
		} catch (NameNotFoundException e) {
			tv.setText(String.format("%s %s", appname,
					"(package or version unknown)"));
		}

	}
}
