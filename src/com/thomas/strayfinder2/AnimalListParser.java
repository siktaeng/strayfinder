package com.thomas.strayfinder2;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

class AnimalListParser extends AsyncTask<Uri, AnimalItem, Exception> {
	Context mContext;

	@Override
	protected Exception doInBackground(Uri... params) {
		Uri url = params[0];
		String list_url = url.toString();
		String base_url = "http://www.animal.go.kr";

		try {
			Document doc = Jsoup.parse(new URL(list_url).openConnection()
					.getInputStream(), "euc-kr", base_url);

			Log.e("MY_TAG", "list_url:" + list_url);

			// traverse table entry with summary attr
			Elements tables = doc.getElementsByAttribute("summary");
			for (Element tbl : tables) {
				AnimalItem ai = new AnimalItem(mContext);

				// Parse thumb nail image
				String img_src = tbl.getElementsByTag("img").first()
						.attr("src");

				// Parse descriptions
				Elements ths = tbl.getElementsByTag("th");
				for (Element th : ths) {
					String key = th.getElementsByTag("img").first().attr("alt");
					String value = th.nextElementSibling().text();
					ai.setDescription(key, value);
				}

				// Parse desertion_no
				String dstr = tbl
						.getElementsByAttributeValueContaining("href",
								"desertion_no").first().attr("href");
				ai.setDesertionNo(dstr.split(".*desertion_no=|&s_date")[1]);

				// Add this item to list
				ai.parsePreview(base_url + img_src);

				publishProgress(ai);
			}

			if (tables.size() == 0)
				return new Exception("No more item found");
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return e;
		} catch (IOException e) {
			e.printStackTrace();
			return e;
		}
		return null;
	}
}