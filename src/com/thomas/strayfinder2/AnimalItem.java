package com.thomas.strayfinder2;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import com.thomas.strayfinder2.R;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

class AnimalItem implements OnClickListener {
	protected Context mContext;

	private final HashMap<String, String> desc = new HashMap<String, String>();
	private String desertion_no;
	private Drawable thumbnail;

	public AnimalItem(Context c) {
		mContext = c;
	}

	public CharSequence getBriefDescription() {
		String status = desc.get("상태");
		if (status.equals("종료(반환)"))
			status = "<font color='#00ffff'>" + status + "</font>";
		else if (status.equals("종료(입양)"))
			status = "<font color='#32ff32'>" + status + "</font>";
		else if (status.equals("종료(안락사)") || status.equals("종료(자연사)"))
			status = "<font color='#ff3232'>" + status + "</font>";

		return TextUtils.concat(desc.get("접수일시"), " ", Html.fromHtml(status),
				"\n", desc.get("성별"), " ", desc.get("품종"), " (",
				desc.get("공고번호"), ")\n", "발견장소: ", desc.get("발견장소"), "\n",
				"특징: ", desc.get("특징"));
	}

	public Drawable getThumbnail() {
		return thumbnail;
	}

	public void onClick(View v) {
		String view_url = mContext.getResources().getString(R.string.view_url)
				+ "?desertion_no=" + desertion_no;
		Intent intent = new Intent(mContext, DetailViewActivity.class);
		intent.putExtra("view_url", view_url);
		intent.putExtra("status", desc.get("상태"));
		mContext.startActivity(intent);
	}

	public void parsePreview(String imgUrl) {
		Log.e("parsePReview", "" + imgUrl);

		try {
			URL url = new URL(imgUrl);

			// Open an HTTP connection to the URL
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setUseCaches(true);
			conn.setDoInput(true); // Allow Inputs
			conn.connect();

			thumbnail = new BitmapDrawable(mContext.getResources(),
					BitmapFactory.decodeStream(conn.getInputStream()));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setDescription(String key, String value) {
		desc.put(key, value);
	}

	public void setDesertionNo(String string) {
		desertion_no = string;
	}
}