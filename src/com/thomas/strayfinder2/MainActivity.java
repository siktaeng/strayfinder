package com.thomas.strayfinder2;

import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.thomas.strayfinder2.R;

//import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.preference.PreferenceManager;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainActivity extends SherlockListActivity implements
		OnSharedPreferenceChangeListener {
	private AnimalsAdapter animalsAdapter;
	private TextView tv = null;

	private boolean active = false;
	private boolean prefChanged = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		tv = (TextView) findViewById(R.id.sub_text);

		animalsAdapter = new AnimalsAdapter(this, 0) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				return getListViewItem(position, convertView, parent);
			}
		};

		PreferenceManager.getDefaultSharedPreferences(this)
				.registerOnSharedPreferenceChangeListener(this);

		if (MyApp.shouldRefresh())
			animalsAdapter.loadMorePage();

		animalsAdapter.registerDataSetObserver(new DataSetObserver() {
			@Override
			public void onChanged() {
				MainActivity.this.updateTitleBar();
				super.onChanged();
			}
		});

		setListAdapter(animalsAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.activity_main, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;

		switch (item.getItemId()) {
		case R.id.menu_settings:
			intent = new Intent(MainActivity.this, SettingsActivity.class);
			MainActivity.this.startActivity(intent);
			break;
		case R.id.menu_refresh:
			refresh();
			break;
		case R.id.menu_about:
			intent = new Intent(MainActivity.this, AboutActivity.class);
			MainActivity.this.startActivity(intent);
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPause() {
		active = false;
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		active = true;

		// If prefChanged and shouldRefresh, do refresh
		if (MyApp.shouldRefresh() && prefChanged)
			refresh();

		prefChanged = false;

		updateTitleBar();
	}

	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		// When activity is not active, just set prefChanged as true and return
		if (!active) {
			prefChanged = true;
			return;
		}

		if (MyApp.shouldRefresh())
			refresh();
	}

	private void refresh() {
		animalsAdapter.clearAll();
		animalsAdapter.loadMorePage();
	}

	private void updateTitleBar() {
		int count = (animalsAdapter == null) ? 0
				: animalsAdapter.getCount() - 1;

		tv.setText(MyApp.getSearchCondition() + " / " + count + "�׸� �˻�");
	}
}