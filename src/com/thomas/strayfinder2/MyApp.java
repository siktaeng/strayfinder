package com.thomas.strayfinder2;

import com.thomas.strayfinder2.R;

import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.preference.PreferenceManager;

public class MyApp extends Application {
	private static Context app;

	public static Context getApp() {
		return app;
	}

	public static String getSearchCondition() {
		String upr = PreferenceManager.getDefaultSharedPreferences(getApp())
				.getString("s_upr_cd", "").split(",")[0];
		String org = PreferenceManager.getDefaultSharedPreferences(getApp())
				.getString("s_org_cd", "").split(",")[0];
		String shelter = PreferenceManager
				.getDefaultSharedPreferences(getApp())
				.getString("s_shelter_cd", "").split(",")[0];
		String up_kind = PreferenceManager
				.getDefaultSharedPreferences(getApp())
				.getString("s_up_kind_cd", "").split(",")[0];
		String kind = PreferenceManager.getDefaultSharedPreferences(getApp())
				.getString("s_kind_cd", "").split(",")[0];

		String str = upr;
		if (!org.equals("선택"))
			str += " " + org;

		if (!shelter.equals("전체"))
			str += " " + shelter;

		str += " / " + up_kind;
		if (!kind.equals("선택"))
			str += " " + kind;

		return str;
	}

	public static Uri getUri() {
		boolean listProtectedOnly = PreferenceManager
				.getDefaultSharedPreferences(getApp()).getBoolean(
						"list_protected_only", false);
		String base_url = getApp().getResources().getString(
				listProtectedOnly ? R.string.protection_list_url
						: R.string.public_list_url);

		Uri uri = Uri.parse(base_url);
		Uri.Builder builder = uri.buildUpon();

		final String[] lists = { "s_upr_cd", "s_org_cd", "s_shelter_cd",
				"s_up_kind_cd", "s_kind_cd" };
		for (String str : lists) {
			String[] values = PreferenceManager
					.getDefaultSharedPreferences(getApp()).getString(str, "")
					.split(",");

			String value;
			if (values.length <= 1 || values[1].equals("0000000")
					|| values[1].equals("null"))
				value = str.equals("s_org_cd") ? "0000000" : "";
			else
				value = values[1];

			builder = builder.appendQueryParameter(str, value);
		}

		uri = builder.build();

		return uri;
	}

	public static boolean isDebugMode() {
		return PreferenceManager.getDefaultSharedPreferences(MyApp.getApp())
				.getBoolean("debug_mode", false);
	}

	public static boolean shouldListProtectedOnly() {
		return PreferenceManager.getDefaultSharedPreferences(MyApp.getApp())
				.getBoolean("list_protected_only", false);
	}

	public static boolean shouldRefresh() {
		return PreferenceManager.getDefaultSharedPreferences(MyApp.getApp())
				.getBoolean("list_refresh", true);
	}

	@Override
	public void onCreate() {
		super.onCreate();

		app = this;
	}
}
