package com.thomas.strayfinder2;

import com.thomas.strayfinder2.R;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public abstract class AnimalsAdapter extends ArrayAdapter<AnimalItem> implements
		AnimalsInterface {
	private int parsedPages = 0;
	private AnimalListParser parseListTask;

	public AnimalsAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
	}

	public void clearAll() {
		clear();

		parsedPages = 0;

		Toast.makeText(getContext(), "All items cleared", Toast.LENGTH_SHORT)
				.show();
	}

	public int DPtoPixel(int DP) {
		final float DEFAULT_HDIP_DENSITY_SCALE = 1.6f;
		float scale = getContext().getResources().getDisplayMetrics().density;

		return (int) (DP * scale / DEFAULT_HDIP_DENSITY_SCALE);
	}

	private void drawListViewItem(int position, LinearLayout ll,
			ProgressBar pb, TextView tv) {
		Drawable d;

		try {
			if (position < getCount() - 1) {
				d = getItem(position).getThumbnail();
				d.setBounds(0, 0, DPtoPixel(150), DPtoPixel(150));
				ll.setGravity(Gravity.CENTER_VERTICAL);
				tv.setText(getItem(position).getBriefDescription());
				pb.setVisibility(View.GONE);
			} else if (isParsing()) {
				d = null;
				ll.setGravity(Gravity.CENTER);
				tv.setText("목록을 불러오는 중");
				pb.setVisibility(View.VISIBLE);
			} else {
				d = getContext().getResources().getDrawable(
						android.R.drawable.ic_menu_more);
				d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
				ll.setGravity(Gravity.CENTER);
				tv.setText("클릭하면 목록을 가져옵니다");
				pb.setVisibility(View.GONE);
			}

			tv.setCompoundDrawables(d, null, null, null);
		} catch (Exception e) {
			Log.e("MMM", "" + e + " with " + position + " / " + getCount());
		}
	}

	@Override
	public int getCount() {
		return super.getCount() + 1;
	}

	public View getListViewItem(final int position, View convertView,
			ViewGroup parent) {
		final LinearLayout ll;
		final ProgressBar pb;
		final TextView tv;
		if (convertView == null) {
			convertView = LinearLayout.inflate(getContext(),
					R.layout.listitem_preview, null);
			ll = (LinearLayout) convertView;
			pb = (ProgressBar) ll.findViewById(R.id.progressbar_preview);
			tv = (TextView) ll.findViewById(R.id.textview_preview);
			convertView.setTag(R.id.progressbar_preview, pb);
			convertView.setTag(R.id.textview_preview, tv);
		} else {
			ll = (LinearLayout) convertView;
			pb = (ProgressBar) convertView.getTag(R.id.progressbar_preview);
			tv = (TextView) convertView.getTag(R.id.textview_preview);
		}

		drawListViewItem(position, ll, pb, tv);

		if (position < getCount() - 1)
			ll.setOnClickListener(getItem(position));
		else if (isParsing())
			ll.setOnClickListener(null);
		else
			ll.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					loadMorePage();
					drawListViewItem(position, ll, pb, tv);
				}
			});

		return ll;
	}

	private boolean isParsing() {
		return (parseListTask != null && parseListTask.getStatus() != AsyncTask.Status.FINISHED);
	}

	public void loadMorePage() {
		if (parseListTask != null
				&& parseListTask.getStatus() != AsyncTask.Status.FINISHED)
			return;

		// Invoke new async task only if previous one is finished or does not
		// exist
		parseListTask = new AnimalListParser() {
			@Override
			protected void onPostExecute(Exception result) {
				super.onPostExecute(result);

				if (result == null) {
					Toast.makeText(mContext, "Page " + parsedPages + " loaded",
							Toast.LENGTH_SHORT).show();
				} else {
					--parsedPages;
					Toast.makeText(mContext, "" + result.getMessage(),
							Toast.LENGTH_SHORT).show();
				}
				notifyDataSetChanged();
			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();

				mContext = AnimalsAdapter.this.getContext();
			}

			@Override
			protected void onProgressUpdate(AnimalItem... values) {
				super.onProgressUpdate(values);

				AnimalsAdapter.this.add(values[0]);
			}
		};

		++parsedPages;

		Uri uri = MyApp.getUri();
		uri = uri.buildUpon().appendQueryParameter("pagecnt", "" + parsedPages)
				.build();

		Log.e("MY_TAG", "Parsing page " + parsedPages + " in background");
		parseListTask.execute(uri);
	}
}

interface AnimalsInterface {
	public abstract void clearAll();

	public abstract View getListViewItem(final int position, View convertView,
			ViewGroup parent);

	public abstract void loadMorePage();
}
